#include "HelloWorldScene.h"
USING_NS_CC;

Scene* HelloWorld::createScene()
{

    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
    auto closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));
    
	closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));

    // create menu, it's an autorelease object
    auto menu = Menu::create(closeItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);
	


    /////////////////////////////
    // 3. add your codes below...

    // add a label shows "Hello World"
    // create and initialize a label
    
    // add "HelloWorld" splash screen"
    //auto sprite = Sprite::create("HelloWorld.png");

    // position the sprite on the center of the screen
    //sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));

    // add the sprite as a child to this layer
    //this->addChild(sprite, 0);
    //auto fir = ParticleFire::create();
    //this->addChild(fir);
	
    Texture2D* texture = Director::getInstance()->getTextureCache()->addImage("air(normal).png");



	auto test = Sprite::create("air(big).png");

	this->addChild(test,1);

	test->setPosition(710,222);

	test->setScale(0.02);

	Air* airtest = Air::create();

	airtest->generateTree();

    for(int i = 0;i<10;i++){
       auto air = AirParticle::create(airtest,texture,Point(300,0.1*i),1,4+0.1*i,100,0);
        this->addChild(air,1);
       air->setPosition(20*i+100,450);
	   air->setScale(0.5);
    }

	Sprite* mete = Sprite::create("star1.png");
	mete->setScale(1.5);
	auto fire = Fire::create(mete,airtest);
	this->addChild(fire);
	fire->setPosition(0,300);
	fire->runAction(MoveBy::create(3,Point(700,0)));

	airtest->setTouchLayer(this);

	this->addChild(airtest);


	log("init successfully");
    return true;
}


void HelloWorld::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement() a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}
